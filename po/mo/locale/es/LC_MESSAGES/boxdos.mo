��    E      D  a   l      �  	   �  
   �  
     
     
        '     /     3     <     N  *   T          �     �     �     �     �     �  
   �     �     �  +   �     *  
   A     L     R  
   b     m     r     ~     �     �     �     �     �  .   �     �     �          
          0     <     B     G     ]     q     �     �  ,   �  &   �     �     �     �     	  
   	     	     #	     1	     ?	     [	     p	     �	     �	  9   �	     �	  
   �	      
  �  
     �     �     �     �     �     �     �     �     �       4        I     O     V     c  $   q     �     �  
   �     �     �  2   �      
     +     =     D     Y     i     o     |     �     �     �     �     �  /   �                    !     *     H     W  	   _     i     �     �     �     �  7   �  +   �     (     1     N     V     h     |     �     �  $   �     �  "   �       !   )  C   K     �     �     �         D      "   C                             :   4         9   7   	   +          -       #   *          1      0                              >       B                         !   (       2       6      E   $          ?   ;       5          =   '           8   )       
               @   %   &   /                 <             .          3   A   ,       2ª Image 2ª Image: 3ª Image: 4ª Image: 5ª Image: Actions Add Add/Edit Aspect correction Ayuda CTRL-F5 -> Save a screenshot. (PNG format) Create Cycles Description Description: Disable expanded memory (ems) Do not close DOSBox. ERROR Executable Explore Files found First of all, select your working directory Full screen resolution Fullscreen Games Games directory Games list Help INFORMATION Image Image: Increase disk size Information Install Installer executable: It is necessary to create a configuration file Juegos Launch List Mapper Maximum 
percentage of cycles: Mount image Name: Next No configuration file No games configured Number of cycles: Open Previous Remember to save the game settings in boxdos Remember: CTRL-F4 Switch between disks Remove Remove screenshot Save Scaling Mode: Screenshot Select Select a file Select a game Select a working directory: Select an image file Select the game executable Setting The name field is required Using multiple files is only supported for cue/iso images Video output Which one? Without description Project-Id-Version: boxdos 0.8
Report-Msgid-Bugs-To: mycenred@gmail.com
PO-Revision-Date: 2020-10-07 11:46+0200
Last-Translator: jose <mycenred@gmail.com>
Language-Team: Spanish - Spain <mycenred@gmail.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
X-Generator: Gtranslator 3.36.0
 2ª Imágen 2ª Imágen: 3ª Imágen: 4ª Imágen: 5ª Imágen: Acciones Añadir Añadir/Editar Corrección de aspecto Help CTRL-F5 -> Guarda captura de pantalla.(Formato PNG
) Crear Ciclos Descripción Descripción: Deshabilitar memoria expandida (ems) No cerrar DOSBox. ERROR Ejecutable Explorar Archivos encontrados Antes de nada, seleccione su directorio de trabajo Resolución de pantalla completa Pantalla completa Juegos Directorio de juegos Lista de juegos Ayuda INFORMACIÓN Imágen Imágen: Aumentar el tamaño del disco Información Instalar Ejecutable del instalador: Es necesario crear un archivo de configuración Games Lanzar Listado Mapeador Porcentaje
máximo de ciclos: Montar imágen Nombre: Siguiente Sin archivo de configuración No hay juegos configurados Número de ciclos: Abrir Anterior Recuerda guardar la configuración del juego en boxdos. Recuerde: CTRL-F4 Para cambiar entre discos Eliminar Eliminar captura de pantalla Guardar Modo de escalado: Captura de pantalla Seleccionar Seleccione un archivo Seleccione un juego Seleccione un directorio de trabajo: Seleccione un archivo de imagen Selecciona el ejecutable del juego Configuración El campo de nombre es obligatorio El uso de varios archivos solo es compatible con imágenes cue/iso. Salida de video ¿Cúal? Sin descripción 
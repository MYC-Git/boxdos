#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from distutils.core import setup
from distutils import dep_util

setup(
	name = "Boxdos",
	version = "0.8.2",
	author = "Jose Antonio Carrascosa García",
	author_email = "mycenred@gmail.com",
	maintainer = "Jose Antonio Carrascosa García",
	maintainer_email = "mycenred@gmail.com",
	url = "https://myc-git.gitlab.io/mycweb/",
	description = "Ayuda gráfica para dosbox.",
	long_description = "Ayuda gráfica para instalar, configurar y lanzar programas en dosbox.",
	download_url = "https://myc-git.gitlab.io/mycweb/",
	packages = ["Boxdos"],
	package_dir = {"Boxdos": "bin"},
	data_files = [("/usr/share/applications", ["share/applications/Boxdos.desktop"]),("/usr/share/locale/es/LC_MESSAGES", ["share/locale/es/LC_MESSAGES/boxdos.mo"]),("/usr/share/pixmaps", ["share/pixmaps/Box2.png"]),("/usr/share/man/man1", ["share/man/man1/Boxdos.1.gz"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/op3.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/op6.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/op5.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/op1.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/op4.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/lanzar.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/fig4.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/fig3.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/fig1.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/fig2.png"]),("/usr/share/doc/boxdos/html/img", ["share/doc/boxdos/html/img/op2.png"]),("/usr/share/doc/boxdos/html", ["share/doc/boxdos/html/boxdos.html"]),("/usr/share/doc/boxdos", ["share/doc/boxdos/changelog.gz"]),("/usr/share/doc/boxdos", ["share/doc/boxdos/copyright"]),("/usr/share/doc/boxdos", ["share/doc/boxdos/COPYING"]),("/usr/share/boxdos/data/img", ["share/boxdos/data/img/captura.png"]),("/usr/share/boxdos/data/img", ["share/boxdos/data/img/Sin_imagen.gif"]),("/usr/share/boxdos/data/img", ["share/boxdos/data/img/splash.png"]),("/usr/share/boxdos/data/img", ["share/boxdos/data/img/Sin_imagen2.gif"]),("/usr/share/boxdos/data/img", ["share/boxdos/data/img/Sin_imagen3.gif"]),("/usr/share/boxdos/data/iconos", ["share/boxdos/data/iconos/icono48.png"]),("/usr/share/boxdos/data/iconos", ["share/boxdos/data/iconos/icono2.png"])],
	license = "GPL-3.0+"
	)
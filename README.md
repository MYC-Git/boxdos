# Boxdos.
Ayuda gráfica para instalar, configurar y lanzar programas en dosbox.

Sólo ha sido probado en Ubuntu 20.04 y Debian 10.
## Instalación:  
Descargue el paquete .deb de la carpeta releases.

Si tiene instalado [stdeb](https://github.com/astraw/stdeb). Descargue o clone el código, abra la carpeta de descarga, descomprímala, entre en la carpeta, abra una terminal en ella y ejecute el siguiente comando:  
python3 setup.py --command-packages=stdeb.command bdist_deb

### Dependencias:
  * python3 (>= 3.5)
  * dosbox
  * python3-wxgtk4.0
  * gettext

# Importante
Boxdos is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.

 Boxdos is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Más información:  

[Web del proyecto](https://myc-git.gitlab.io/mycweb/paginas/python/boxdos.html)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2019 - 2025 (C) http://www.mycsoft.tk (Jose Antonio Carrascosa García)
#
# This file is part of Boxdos
#
# Boxdos is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 3 of the License, or 
# (at your option) any later version.
#
# PytoDeb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import wx

que_hago = "nada"

class aviso(wx.MessageDialog):
	def __init__(self, parent, titulo, mensaje, estilo):
		wx.MessageDialog.__init__(self, parent, titulo, mensaje, estilo)

class mens(wx.Dialog):
	
	def __init__(self, parent, titulo, mensaje, op1, op2):
		global que_hago
		que_hago = "nada"
		wx.Dialog.__init__(self, parent, title = titulo)
		cajavmns0 = wx.BoxSizer(wx.VERTICAL)
		caja_mnsv0 = self.CreateTextSizer(mensaje)

		self.bt_integ = wx.Button(self, -1, op1)
		self.bt_crearlo = wx.Button(self, -1, op2)
		sizers = self.CreateButtonSizer(wx.CANCEL)
		sizers.Add(self.bt_integ, 0, wx.EXPAND | wx.ALL, 5)
		sizers.Add(self.bt_crearlo, 0, wx.EXPAND | wx.ALL, 5)
		
		cajavmns0.Add(caja_mnsv0, 2, wx.ALIGN_CENTER | wx.ALL, 5)
		cajavmns0.Add(sizers, 0, wx.EXPAND | wx.TOP, 5)
		self.SetSizer(cajavmns0)

		self.Bind(wx.EVT_BUTTON, self.Click_bt_integ, self.bt_integ)
		self.Bind(wx.EVT_BUTTON, self.Click_bt_crearlo, self.bt_crearlo)
		
	def Click_bt_integ(self, event):
		global que_hago
		que_hago = "integrar"
		self.Close(True)
	def Click_bt_crearlo(self, event):
		global que_hago
		que_hago = "crear"
		self.Close(True)


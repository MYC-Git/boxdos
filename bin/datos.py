#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2019 - 2025 (C) https://myc-git.gitlab.io/mycweb/ (Jose Antonio Carrascosa García)
#
# This file is part of Boxdos
#
# Boxdos is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 3 of the License, or 
# (at your option) any later version.
#
# Boxdos is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

class dato():
	
	def __init__(self, directorio):
		self.dir_busc = directorio
		self.archivos_datos = []
		self.texto = ""

	def entupla(self):
		#volver = os.getcwd()
		#os.chdir(inicio)
		self.archivos_datos = []
		self.lista = []
		for dirpath, dirnames, filenames in os.walk(self.dir_busc, topdown = False):
			if len(filenames) > 0:
				for a in filenames:
					#lista = []
					destino = "/usr/" + dirpath
					origen = dirpath + "/" + a
					self.lista.append(origen)
					tupla = (destino, self.lista)
					self.archivos_datos.append(tupla)
		#os.chdir(volver)
		return self.archivos_datos

	def enlineas(self):
		#volver = os.getcwd()
		#os.chdir(inicio)
		#self.texto = ""
		for dirpath, dirnames, filenames in os.walk(self.dir_busc, topdown = False):
			if len(filenames) > 0:
				for a in filenames:
					destino = "/usr/" + dirpath
					origen = dirpath + "/" + a
					self.texto += destino + "\n" + origen + "\n"
					#print(self.texto)
		#os.chdir(volver)

	def solo_arch(self):
		#volver = os.getcwd()
		#os.chdir(inicio)
		self.archivos_datos = []
		for dirpath, dirnames, filenames in os.walk(self.dir_busc, topdown = False):
			if len(filenames) > 0:
				for a in filenames:
					self.archivos_datos.append(a)
		print(self.archivos_datos)
		#os.chdir(volver)


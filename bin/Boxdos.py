#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2019 - 2025 (C) https://myc-git.gitlab.io/mycweb/ (Jose Antonio Carrascosa García)
#
# This file is part of Boxdos
#
# Boxdos is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 3 of the License, or 
# (at your option) any later version.
#
# Boxdos is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import wx
import wx.adv
import os
import pickle
import wx.lib.agw.advancedsplash as AS
import configparser
import shlex
import subprocess
import gettext
import locale
from random import choice

import instalar
import datos
import mensaje
import ayuda

entorno_usu = locale.setlocale(locale.LC_ALL, '')
idiomas =  [entorno_usu]

dicci_Juegos = []
confi_Juegos = ["", []]
cadena = []
inicio = ""
aumentos = ["512", "768", "1024", "1280", "1536", "1762", "2048"]
class Juegos(wx.Panel):
	def __init__(self, parent, nombre_hoja):
		wx.Panel.__init__(self, parent)

		sizer0 = wx.StaticBoxSizer(wx.HORIZONTAL, self)
		self.cajaBotones = wx.StaticBoxSizer(wx.VERTICAL, self, _("Actions"))
		self.cajaJuegos = wx.StaticBoxSizer(wx.HORIZONTAL, self, _("Games list"))
		self.cajaVerCapt = wx.StaticBoxSizer(wx.VERTICAL, self, _("Screenshot"))
		self.picture = wx.StaticBitmap(self)
		self.animaciones = ("/usr/share/boxdos/data/img/Sin_imagen.gif", "/usr/share/boxdos/data/img/Sin_imagen2.gif", "/usr/share/boxdos/data/img/Sin_imagen3.gif")
		self.anim = wx.adv.Animation('/usr/share/boxdos/data/img/Sin_imagen3.gif')
		self.animctrl = wx.adv.AnimationCtrl(self, -1, self.anim, size=(480, 300))

		self.botonEjecutar = wx.Button(self, -1, label = _("Launch"))
		self.botonEliminar = wx.Button(self, -1, label = _("Remove"))
		self.bt_trabajo = wx.Button(self, -1, _("Games directory"))
		self.bt_BorrarCapt = wx.Button(self, -1, label = _("Remove screenshot"))

		global dicci_Juegos
		global confi_Juegos
		global inicio
		global cadena
		global confiRuta
		global confiDir
		self.parent = parent

		ini = os.getcwd()
		usuario = os.getlogin()
		confiDir = "/home/" + usuario + "/.boxdos"
		confiRuta = "/home/" + usuario + "/.boxdos/boxDos.conf"
		print(confiRuta)
		if os.path.exists(confiRuta):
			configuracion = open(confiRuta, "rb")
			cadena = pickle.load(configuracion)
			configuracion.close()
			inicio = cadena[0]
			dicci_Juegos = cadena[1]
			mensjIni = _("Select a game")
		else:
			inicio = ini
			mensjIni = _("No games configured")
			if os.path.exists(confiDir):
				pass
			else:
				os.mkdir(confiDir)

		i = 0
		self.listado = wx.ListCtrl(self, -1, style=wx.LC_REPORT|wx.LC_SINGLE_SEL)
		self.listado.InsertColumn(0, _('Games'))
		self.listado.SetColumnWidth(0, 300)
		self.descrip = wx.TextCtrl(self, -1, mensjIni, size=(100, 50), style=wx.TE_MULTILINE | wx.TE_READONLY | wx.LC_SORT_ASCENDING)
		self.verCapt = wx.TextCtrl(self, -1, mensjIni, size=(100, 50), style=wx.TE_MULTILINE | wx.TE_READONLY)

		self.cajaBotones.Add(self.botonEjecutar, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaBotones.Add(self.botonEliminar, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaBotones.Add(self.bt_trabajo, 0, wx.EXPAND | wx.ALL, 5)

		self.cajaJuegos.Add(self.listado, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaVerCapt.Add(self.animctrl, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaVerCapt.Add(self.picture, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaVerCapt.Add(self.bt_BorrarCapt, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaVerCapt.Add(self.verCapt, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaVerCapt.Add(self.descrip, 0, wx.EXPAND | wx.ALL, 5)

		self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.SelecJuego)
		self.botonEliminar.Bind(wx.EVT_BUTTON, self.Actualizarr)
		self.botonEjecutar.Bind(wx.EVT_BUTTON, self.Lanzar)
		self.bt_BorrarCapt.Bind(wx.EVT_BUTTON, self.Borrar_Captura)
		#Ejemplo de como enviar parametros a la función llamada con el boton, mediante lambda
		tururu = "Hola caracola"
		self.bt_trabajo.Bind(wx.EVT_BUTTON, lambda event, param = tururu: self.Click_bt_trabajo(event, param))

		self.capturas("/usr/share/boxdos/data/img/captura.png")

		sizer0.Add(self.cajaBotones,0,wx.ALL|wx.EXPAND|wx.ALL,5)
		sizer0.Add(self.cajaJuegos,0,wx.ALL|wx.EXPAND|wx.ALL,5)
		sizer0.Add(self.cajaVerCapt,0,wx.ALL|wx.EXPAND|wx.ALL,5)

		self.SetSizer(sizer0)
		self.cajaVerCapt.Show(self.picture, False)
		self.cajaVerCapt.Show(self.bt_BorrarCapt, False)
		self.cajaVerCapt.Layout()
		self.animctrl.Play()
		self.lanzador = ""
		i = 0
		while i < len(dicci_Juegos):
			self.listado.InsertItem(0, dicci_Juegos[i].get("nombre"))
			i += 1
	def Borrar_Captura(self, event):
		Cap = self.verCapt.GetValue()
		DirCapt = os.path.dirname(Cap)

		try:
			os.remove(Cap)
			ListaCapts = os.listdir(DirCapt)
			if len(ListaCapts) < 1:
				os.rmdir(DirCapt)
				self.cajaVerCapt.Show(self.picture, False)
				self.cajaVerCapt.Show(self.animctrl, True)
				self.cajaVerCapt.Show(self.bt_BorrarCapt, False)
				self.verCapt.SetValue(_("CTRL-F5 -> Save a screenshot. (PNG format)"))
				self.cajaVerCapt.Layout()
			else:
				arch_imagenes = datos.dato(DirCapt)
				arch_imagenes.entupla()
				tex = arch_imagenes.lista
				self.captu = choice(tex)
				self.capturas(self.captu)
				self.verCapt.SetValue(self.captu)
				self.cajaVerCapt.Layout()
		except OSError as e:
			print("Error: %s : %s" % (DirCapt, e.strerror))

	def capturas(self, foto):
		imgCap = wx.Bitmap(foto)
		CapImg = imgCap.ConvertToImage()
		CapImg.Rescale(480, 300)
		Cap = CapImg.ConvertToBitmap()
		self.picture.SetBitmap(Cap)

	def Click_bt_trabajo(self, event, param):
		dlg = wx.DirDialog(self, _("Select a working directory:"), os.getcwd(), style=wx.DD_DEFAULT_STYLE | wx.DD_NEW_DIR_BUTTON)
		global inicio
		global dirs
		if dlg.ShowModal() == wx.ID_OK:
			inicio = dlg.GetPath()
			confi_Juegos[0] = inicio
			confi_Juegos[1] = dicci_Juegos
			configuracion = open(confiRuta, "wb")
			pickle.dump(confi_Juegos, configuracion)
			configuracion.close()
		dlg.Destroy()
		dirs = []

	def Actualizar(self, event):
		self.listado.DeleteAllItems()
		self.descrip.Clear()
		self.verCapt.Clear()
		global dicci_Juegos
		i = 0
		while i < len(dicci_Juegos):
			for clave_juegos in dicci_Juegos[i]:
				if clave_juegos == "nombre":
					self.listado.InsertItem(0, dicci_Juegos[i].get("nombre"))
			i += 1

	def Actualizarr(self, event):
		indice = event.GetSelection()
		juego = self.listado.GetFirstSelected(indice)
		nomJuego = self.listado.GetItemText(juego)
		control = False
		control2 = True
		self.parent.GetPage(1).Borrar(nomJuego, control, control2)

	def SelecJuego(self, event):
		indice = event.GetSelection()
		juego = self.listado.GetFocusedItem()
		nomJuego = self.listado.GetItemText(juego)
		i = 0
		while i < len(dicci_Juegos):
			for valor in dicci_Juegos[i].values():
				if valor == nomJuego:
					self.descrip.SetValue(dicci_Juegos[i].get("descripcion"))
					self.lanzador = dicci_Juegos[i].get("ruconf")
					rutacapt = dicci_Juegos[i].get("ruta")
					rutacapt = os.path.dirname(rutacapt)
					rutacapt = rutacapt + "/capturas"
					if os.path.exists(rutacapt):
						self.cajaVerCapt.Show(self.animctrl, False)
						self.cajaVerCapt.Show(self.picture, True)
						self.cajaVerCapt.Show(self.bt_BorrarCapt, True)
						arch_imagenes = datos.dato(rutacapt)
						arch_imagenes.enlineas()
						tex = arch_imagenes.texto
						arch_imagenes.entupla()
						tex = arch_imagenes.lista
						self.captu = choice(tex)
						self.capturas(self.captu)
						self.verCapt.SetValue(self.captu)
						self.cajaVerCapt.Layout()
					else:
						self.cajaVerCapt.Show(self.picture, False)
						self.cajaVerCapt.Show(self.animctrl, True)
						self.cajaVerCapt.Show(self.bt_BorrarCapt, False)
						animacion = choice(self.animaciones)
						self.animctrl.LoadFile(animacion)
						self.cajaVerCapt.Layout()
						self.animctrl.Play()
						self.verCapt.SetValue(_("CTRL-F5 -> Save a screenshot. (PNG format)"))
			i += 1

	def Lanzar(self, event):
		if self.lanzador == "" or self.lanzador == _("No configuration file"):
			if self.lanzador == "":
				wx.MessageBox(message = _("Select a game"), caption=_('Help'), style=wx.OK | wx.ICON_INFORMATION)
			elif self.lanzador == _("No configuration file"):
				wx.MessageBox(message = _("It is necessary to create a configuration file"), caption=_('Help'), style=wx.OK | wx.ICON_INFORMATION)
		else:
			comando = "dosbox -conf " + inicio + "/dosbox-0.74.conf -conf " + self.lanzador
			args = shlex.split(comando)
			subprocess.call(args, shell=False)

class AgregarBorrar(wx.Panel):
	def __init__(self, parent, nombre_hoja):
		# Constructor.
		wx.Panel.__init__(self, parent)
		resoluciones = ["original", "desktop"]
		salidas = ["surface", "overlay", "opengl", "openglnb", "ddraw"]
		escalas = ["none", "normal2x", "normal3x", "advmame2x", "advmame3x", "advinterp2x", "advinterp3x", "hq2x", "hq3x", "2xsai", "super2xsai", "supereagle", "tv2x", "tv3x", "rgb2x", "rgb3x", "scan2x", "scan3x"]
		ciclos = ["auto", "fixed", "max"]
		codigos = ["be", "br", "cf", "cz", "sl", "dk", "su", "fr", "gr", "hu", "it", "la", "nl", "no", "pl", "po", "ru", "sp", "sv", "sf", "sg", "uk", "us", "dv103", "yu"]
		paises = ["Belgium", "Brazil", "Canadian-French", "Czechoslovakia (Czech)", "Czechoslovakia (Slovak)", "Denmark", "Finland", "France", "Germany", "Hungary", "Italy", "Latin America", "Netherlands", "Norway", "Poland", "Portugal", "Russian", "Spain", "Sweden", "Switzerland (French)", "Switzerland (German)", "United Kingdom", "United States", "United States (Dvorak)", "Yugoslavia (Serbo-Croatian)"]

		i = 0
		kb = []
		self.parent = parent
		while i < len(codigos):
			a = codigos[i] + "-->" + paises[i]
			kb.append(a)
			i +=1
		i = 0
		global dicci_Juegos

		# Sizers.
		sizer1 = wx.StaticBoxSizer(wx.HORIZONTAL, self)
		self.cajaJuegos1 = wx.StaticBoxSizer(wx.HORIZONTAL, self, _("Games list"))
		self.cajaAgregar = wx.StaticBoxSizer(wx.VERTICAL, self, _("Add"))

		self.cajaAgregar1 = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaAgregar2 = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaAgregar3 = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaAgregar4 = wx.BoxSizer(wx.HORIZONTAL)

		self.cajaConf0 = wx.StaticBoxSizer(wx.VERTICAL, self, "SDL")
		self.cajaResol = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaSalida = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaJoyst = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaConf2 = wx.StaticBoxSizer(wx.VERTICAL, self, "Render")
		self.cajaConf3 = wx.StaticBoxSizer(wx.VERTICAL, self, "Cpu")
		self.cajaConf4 = wx.StaticBoxSizer(wx.VERTICAL, self, "Dos")
		self.cajaConf5 = wx.StaticBoxSizer(wx.VERTICAL, self, "Autoexec")
		self.cajaCiclos = wx.BoxSizer(wx.VERTICAL)
		self.cajaCiclosMax = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaCiclosFijo = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaIso = wx.BoxSizer(wx.VERTICAL)
		self.cajaAumentarConf = wx.BoxSizer(wx.HORIZONTAL)
		self.subCajaIso = wx.BoxSizer(wx.HORIZONTAL)
		self.subCajaIso3 = wx.BoxSizer(wx.VERTICAL)

		self.s_textNombre = wx.StaticText(self, -1, _("Name:"))
		self.textNombre = wx.TextCtrl(self, -1, size = (210, 35))
		self.s_textDescrip = wx.StaticText(self, -1, _("Description:"))
		self.textDescrip = wx.TextCtrl(self, -1, size = (210, 35))
		self.s_textRuta = wx.StaticText(self, -1, _("Executable"))
		self.textRuta = wx.TextCtrl(self, -1, size = (150, 35))
		self.ch_FullScr = wx.CheckBox(self, -1, _("Fullscreen"))
		self.s_textFullRes = wx.StaticText(self, -1, _("Full screen resolution"))
		self.cb_Resol = wx.ComboBox(self, -1, value = resoluciones[0], choices = resoluciones, style=wx.CB_READONLY, size = (150, 35))
		self.s_textSalida = wx.StaticText(self, -1, _("Video output"))
		self.cb_Salida = wx.ComboBox(self, -1, value = salidas[0], choices = salidas, style=wx.CB_READONLY, size = (150, 35))
		self.ch_Mapeo = wx.CheckBox(self, -1, _("Mapper"))
		self.bt_Crmap = wx.Button(self, -1, label = _("Create"))
		self.ch_Aspect = wx.CheckBox(self, -1, _("Aspect correction"))
		self.s_textEscalado = wx.StaticText(self, -1, _("Scaling Mode:"))
		self.cb_Escalado = wx.ComboBox(self, -1, value = escalas[1], choices = escalas, style=wx.CB_READONLY, size = (150, 35))
		self.s_textCiclos = wx.StaticText(self, -1, _("Cycles"))
		self.cb_Ciclos = wx.ComboBox(self, -1, value = ciclos[0], choices = ciclos, style=wx.CB_READONLY, size = (150, 35))
		self.ch_Ems = wx.CheckBox(self, -1, _("Disable expanded memory (ems)"))
		self.cb_Kb = wx.ComboBox(self, -1, value = kb[17], choices = kb, style=wx.CB_READONLY, size = (150, 35))
		self.s_textCicMax = wx.StaticText(self, -1, _("Maximum \npercentage of cycles:"))
		self.textCicMax = wx.TextCtrl(self, -1, size = (210, 30))
		self.s_textFijo = wx.StaticText(self, -1, _("Number of cycles:"))
		self.textFijo = wx.TextCtrl(self, -1, size = (210, 30))
		self.ch_Montar = wx.CheckBox(self, -1, _("Mount image"))
		self.s_textIso = wx.StaticText(self, -1, _("Image"))
		self.textIso = wx.TextCtrl(self, -1, size = (130, 30))

		self.ch_AumentarConf = wx.CheckBox(self, -1, _("Increase disk size"))
		self.cb_AumentarConf = wx.ComboBox(self, -1, value = aumentos[0], choices = aumentos, style=wx.CB_READONLY, size = (150, 35))

		self.bt_Agregar = wx.Button(self, -1, label = _("Open"))
		self.bt_Guardar = wx.Button(self, -1, label = _("Save"))
		self.bt_Iso = wx.Button(self, -1, label = _("Select"))
		self.bt_Sig = wx.Button(self, -1, label = _("Next"))
		self.bt_Ant = wx.Button(self, -1, label = _("Previous"))

		self.cajaAgregar1.Add(self.s_textNombre, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar1.Add(self.textNombre, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar2.Add(self.s_textDescrip, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar2.Add(self.textDescrip, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar3.Add(self.s_textRuta, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaAgregar3.Add(self.textRuta, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaAgregar3.Add(self.bt_Agregar, 0, wx.EXPAND | wx.ALL, 5)

		self.cajaAgregar4.Add(self.bt_Ant, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaAgregar4.Add(self.bt_Sig, 0, wx.EXPAND | wx.ALL, 5)
		self.cajaAgregar4.Add(self.bt_Guardar, 0, wx.EXPAND | wx.ALL, 5)

		self.cajaResol.Add(self.s_textFullRes, 0, wx.EXPAND|wx.ALL,5)
		self.cajaResol.Add(self.cb_Resol, 0, wx.EXPAND|wx.ALL,5)

		self.cajaSalida.Add(self.s_textSalida, 0, wx.EXPAND|wx.ALL,5)
		self.cajaSalida.Add(self.cb_Salida, 0, wx.EXPAND|wx.ALL,5)
		self.cajaJoyst.Add(self.bt_Crmap, 0, wx.EXPAND|wx.ALL,5)
		self.cajaCiclos.Add(self.s_textCiclos, 0, wx.EXPAND|wx.ALL,5)
		self.cajaCiclos.Add(self.cb_Ciclos, 0, wx.EXPAND|wx.ALL,5)
		self.cajaCiclosMax.Add(self.s_textCicMax, 0, wx.EXPAND|wx.ALL,5)
		self.cajaCiclosMax.Add(self.textCicMax, 0, wx.EXPAND|wx.ALL,5)
		self.cajaCiclosFijo.Add(self.s_textFijo, 0, wx.EXPAND|wx.ALL,5)
		self.cajaCiclosFijo.Add(self.textFijo, 0, wx.EXPAND|wx.ALL,5)
		self.cajaIso.Add(self.ch_Montar, 0, wx.EXPAND|wx.ALL,5)

		self.subCajaIso3.Add(self.ch_AumentarConf, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAumentarConf.Add(self.cb_AumentarConf, 0, wx.EXPAND|wx.ALL,5)

		self.subCajaIso.Add(self.s_textIso, 0, wx.EXPAND|wx.ALL,5)
		self.subCajaIso.Add(self.textIso, 0, wx.EXPAND|wx.ALL,5)
		self.subCajaIso.Add(self.bt_Iso, 0, wx.EXPAND|wx.ALL,5)

		self.subCajaIso3.Add(self.cajaAumentarConf, 0, wx.EXPAND|wx.ALL,5)

		self.cajaIso.Add(self.subCajaIso, 0, wx.EXPAND|wx.ALL,5)
		self.cajaIso.Add(self.subCajaIso3, 0, wx.EXPAND|wx.ALL,5)

		self.cajaConf0.Add(self.ch_FullScr, 0, wx.EXPAND|wx.ALL,5)
		self.cajaConf0.Add(self.cajaResol, 0, wx.EXPAND|wx.ALL,5)
		self.cajaConf0.Add(self.cajaSalida, 0, wx.EXPAND|wx.ALL,5)
		self.cajaConf0.Add(self.ch_Mapeo, 0, wx.EXPAND|wx.ALL,5)
		self.cajaConf0.Add(self.cajaJoyst, 0, wx.EXPAND|wx.ALL,5)

		self.cajaConf2.Add(self.ch_Aspect, 0, wx.EXPAND|wx.ALL,5)
		self.cajaConf2.Add(self.s_textEscalado, 0, wx.EXPAND|wx.ALL,5)
		self.cajaConf2.Add(self.cb_Escalado, 0, wx.EXPAND|wx.ALL,5)

		self.cajaConf3.Add(self.cajaCiclos, 0, wx.EXPAND|wx.ALL,5)

		self.cajaConf4.Add(self.ch_Ems, 0, wx.EXPAND|wx.ALL,5)
		self.cajaConf4.Add(self.cb_Kb, 0, wx.EXPAND|wx.ALL,5)

		self.cajaConf5.Add(self.cajaIso, 0, wx.EXPAND|wx.ALL,5)
		self.cajaCiclos.Add(self.cajaCiclosMax, 0, wx.EXPAND|wx.ALL,5)
		self.cajaCiclos.Add(self.cajaCiclosFijo, 0, wx.EXPAND|wx.ALL,5)

		self.cajaAgregar.Add(self.cajaAgregar1, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar.Add(self.cajaAgregar2, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar.Add(self.cajaAgregar3, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar.Add(self.cajaConf0, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar.Add(self.cajaConf2, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar.Add(self.cajaConf3, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar.Add(self.cajaConf4, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar.Add(self.cajaConf5, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAgregar.Add(self.cajaAgregar4, 0, wx.EXPAND|wx.ALL,5)

		if os.path.exists(confiRuta):
			dicci_Juegos = cadena[1]
		else:
			mensjIni = _("No games configured")

		i = 0
		self.listado = wx.ListCtrl(self, -1, style=wx.LC_REPORT|wx.LC_SINGLE_SEL)
		self.listado.InsertColumn(0, _("Games"))
		self.listado.SetColumnWidth(0, 325)
		self.cajaJuegos1.Add(self.listado, 0, wx.EXPAND | wx.ALL, 5)
		self.bt_Agregar.Bind(wx.EVT_BUTTON, self.Agregar)
		self.bt_Guardar.Bind(wx.EVT_BUTTON, self.Guardar)
		self.Bind(wx.EVT_CHECKBOX, self.VerResol, self.ch_FullScr)
		self.Bind(wx.EVT_CHECKBOX, self.VerMapeo, self.ch_Mapeo)
		self.Bind(wx.EVT_BUTTON, self.CrMapeo, self.bt_Crmap)
		self.Bind(wx.EVT_COMBOBOX, self.VerCiclos, self.cb_Ciclos)
		self.Bind(wx.EVT_CHECKBOX, self.MasDisco, self.ch_AumentarConf)
		self.Bind(wx.EVT_CHECKBOX, self.MontarImagen, self.ch_Montar)
		self.bt_Sig.Bind(wx.EVT_BUTTON, self.Cambiar)
		self.bt_Ant.Bind(wx.EVT_BUTTON, self.Cambiar)
		self.bt_Iso.Bind(wx.EVT_BUTTON, self.SelImagen)
		self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.SelJuego)

		sizer1.Add(self.cajaJuegos1, 0, wx.ALL|wx.EXPAND|wx.ALL,5)
		sizer1.Add(self.cajaAgregar, 2, wx.ALL|wx.EXPAND|wx.ALL,5)
		

		self.SetSizer(sizer1)

		i = 0
		while i < len(dicci_Juegos):
			self.listado.InsertItem(0, dicci_Juegos[i].get("nombre"))
			i += 1

		self.ejecu = ""
		self.rutaejec = ""
		self.rutaConf = ""
		self.control = False

		self.cajaConf0.Show(self.cajaResol, False)
		self.cajaConf0.Show(self.cajaJoyst, False)
		self.cajaCiclos.Show(self.cajaCiclosMax, False)
		self.cajaCiclos.Show(self.cajaCiclosFijo, False)
		self.cajaAgregar4.Show(self.bt_Ant, False)
		self.cajaAgregar4.Show(self.bt_Guardar, False)
		self.cajaAgregar.Show(self.cajaConf0, False)
		self.cajaAgregar.Show(self.cajaConf2, False)
		self.cajaAgregar.Show(self.cajaConf3, False)
		self.cajaAgregar.Show(self.cajaConf4, False)
		self.cajaAgregar.Show(self.cajaConf5, False)

	def CrMapeo(self, event):
		confTemp = configparser.RawConfigParser()
		dicTemp = {}
		dicTemp.update({"mapperfile": confiDir + "/temporal.map"})
		confTemp["sdl"] = dicTemp
		rutaTemp = confiDir + "/temporal.conf"

		autoexec = "[autoexec]\n"
		autoexec += "exit"


		with open(rutaTemp, 'w') as archivo:
			confTemp.write(archivo)
		with open(rutaTemp, 'a') as archivo:
			archivo.write(autoexec)
		comando = "dosbox -conf dosbox-0.74.conf -conf " + rutaTemp + " -startmapper"
		wx.Shell(comando)
		os.remove(rutaTemp)
		if os.path.exists(confiDir + "/temporal.map"):
			dlg_aviso = mensaje.aviso(self, _("Remember to save the game settings in boxdos"), _("INFORMATION"), wx.OK|wx.ICON_INFORMATION)
			dlg_aviso.ShowModal()
			dlg_aviso.Destroy()
		else:
			self.ch_Mapeo.SetValue(False)
			self.cajaConf0.Show(self.cajaJoyst, False)
			self.cajaConf0.Layout()
			self.cajaAgregar.Layout()

	def MontarImagen(self, event):
		if self.ch_Montar.GetValue() ==True:
			self.cajaIso.Show(self.subCajaIso, True)
		else:
			self.cajaIso.Show(self.subCajaIso, False)
		self.cajaAgregar.Layout()

	def MasDisco(self, event):
		ch_IdCh = event.GetEventObject().GetId()
		ch_IdConf = self.ch_AumentarConf.GetId()
		ch_IdIns = self.parent.GetPage(2).ch_Aumentar.GetId()
		if ch_IdCh == ch_IdIns:
			if self.parent.GetPage(2).ch_Aumentar.GetValue() == True:
				self.parent.GetPage(2).subcajaInstalar6.Show(self.parent.GetPage(2).cajaAumentarins, True)
			else:
				self.parent.GetPage(2).subcajaInstalar6.Show(self.parent.GetPage(2).cajaAumentarins, False)
			self.parent.GetPage(2).cajaInstalar.Layout()
		if ch_IdCh == ch_IdConf:
			if self.ch_AumentarConf.GetValue() == True:
				self.subCajaIso3.Show(self.cajaAumentarConf, True)
			else:
				self.subCajaIso3.Show(self.cajaAumentarConf, False)
			self.cajaAgregar.Layout()

	def SelJuego(self, event):
		indice = event.GetSelection()
		juego = self.listado.GetFocusedItem()
		nomJuego = self.listado.GetItemText(juego)
		i = 0
		while i < len(dicci_Juegos):
			for valor in dicci_Juegos[i].values():
				if valor == nomJuego:
					bolFull = dicci_Juegos[i].get("fullscr") in ("True")
					bolAspect = dicci_Juegos[i].get("aspecto") in ("True")
					bolEms = dicci_Juegos[i].get("Ems") in ("True")
					bolMontar = dicci_Juegos[i].get("montar") in ("True")
					bolEspacio = dicci_Juegos[i].get("Espacio") in ("True")
					bolMap = dicci_Juegos[i].get("mapear") in ("True")

					self.textNombre.SetValue(dicci_Juegos[i].get("nombre"))
					self.textDescrip.SetValue(dicci_Juegos[i].get("descripcion"))
					self.textRuta.SetValue(dicci_Juegos[i].get("ruta"))
					self.ch_FullScr.SetValue(bolFull)
					self.cb_Resol.SetValue(dicci_Juegos[i].get("resolucion"))
					self.cb_Salida.SetValue(dicci_Juegos[i].get("salida"))
					self.ch_Aspect.SetValue(bolAspect)
					self.cb_Escalado.SetValue(dicci_Juegos[i].get("escalado"))
					self.cb_Ciclos.SetValue(dicci_Juegos[i].get("ciclos"))
					self.textCicMax.SetValue(dicci_Juegos[i].get("maxciclos"))
					self.textFijo.SetValue(dicci_Juegos[i].get("fijciclos"))
					self.cb_Kb.SetValue(dicci_Juegos[i].get("mapateclas"))
					self.ch_Ems.SetValue(bolEms)
					self.ch_Montar.SetValue(bolMontar)
					self.textIso.SetValue(dicci_Juegos[i].get("imagen"))
					self.ch_AumentarConf.SetValue(bolEspacio)
					self.cb_AumentarConf.SetValue(dicci_Juegos[i].get("EspacioDisco"))
					
					self.ch_Mapeo.SetValue(bolMap)
					if bolMap == True and self.cajaAgregar.IsShown(self.cajaConf0) == True:
						self.cajaConf0.Show(self.cajaJoyst, True)
					else:
						self.cajaConf0.Show(self.cajaJoyst, False)
					if bolMontar == True and self.cajaAgregar.IsShown(self.cajaConf5) ==True:
						self.cajaIso.Show(self.subCajaIso, True)
					else:
						self.cajaIso.Show(self.subCajaIso, False)
					if bolEspacio == True and self.cajaAgregar.IsShown(self.cajaConf5) ==True:
						self.subCajaIso3.Show(self.cajaAumentarConf, True)
					else:
						self.subCajaIso3.Show(self.cajaAumentarConf, False)

					self.cajaConf0.Layout()
					self.cajaAgregar.Layout()
			i += 1

	def Cambiar(self, event):
		bt_IdNav = event.GetEventObject().GetId()
		bt_IdAnt = self.bt_Ant.GetId()
		bt_IdSig = self.bt_Sig.GetId()
		valorCiclos = self.cb_Ciclos.GetValue()
		pantComp = self.ch_FullScr.GetValue()
		mapear = self.ch_Mapeo.GetValue()
		if self.cajaAgregar.IsShown(self.cajaAgregar1) == True:
			self.cajaAgregar.Show(self.cajaAgregar1, False)
			self.cajaAgregar.Show(self.cajaAgregar2, False)
			self.cajaAgregar.Show(self.cajaAgregar3, False)
			self.cajaAgregar.Show(self.cajaConf0, True)
			self.cajaAgregar.Show(self.cajaConf2, True)
			self.cajaAgregar4.Show(self.bt_Ant, True)
			if pantComp != True:
				self.cajaConf0.Show(self.cajaResol, False)
			if mapear != True:
				self.cajaConf0.Show(self.cajaJoyst, False)
			self.cajaConf0.Layout()
			self.cajaAgregar.Layout()

		elif self.cajaAgregar.IsShown(self.cajaConf0) == True and bt_IdNav == bt_IdAnt:
			self.cajaAgregar.Show(self.cajaConf0, False)
			self.cajaAgregar.Show(self.cajaConf2, False)
			self.cajaAgregar4.Show(self.bt_Ant, False)
			self.cajaAgregar.Show(self.cajaAgregar1, True)
			self.cajaAgregar.Show(self.cajaAgregar2, True)
			self.cajaAgregar.Show(self.cajaAgregar3, True)
			self.cajaAgregar.Layout()
			
		elif self.cajaAgregar.IsShown(self.cajaConf0) == True and bt_IdNav == bt_IdSig:
			self.cajaAgregar.Show(self.cajaConf0, False)
			self.cajaAgregar.Show(self.cajaConf2, False)
			self.cajaAgregar4.Show(self.bt_Sig, False)
			self.cajaAgregar.Show(self.cajaConf3, True)
			self.cajaAgregar.Show(self.cajaConf4, True)
			self.cajaAgregar.Show(self.cajaConf5, True)
			self.cajaAgregar4.Show(self.bt_Guardar, True)

			if valorCiclos == "auto":
				self.cajaCiclos.Show(self.cajaCiclosMax, False)
				self.cajaCiclos.Show(self.cajaCiclosFijo, False)
			elif valorCiclos == "max":
				self.cajaCiclos.Show(self.cajaCiclosFijo, False)
				self.cajaCiclos.Show(self.cajaCiclosMax, True)
			elif valorCiclos == "fixed":
				self.cajaCiclos.Show(self.cajaCiclosMax, False)
				self.cajaCiclos.Show(self.cajaCiclosFijo, True)
			if self.ch_AumentarConf.GetValue() == True:
				self.subCajaIso3.Show(self.cajaAumentarConf, True)
			else:
				self.subCajaIso3.Show(self.cajaAumentarConf, False)
			if self.ch_Montar.GetValue() == True:
				self.cajaIso.Show(self.subCajaIso, True)
			else:
				self.cajaIso.Show(self.subCajaIso, False)
			self.cajaAgregar4.Layout()
			self.cajaAgregar.Layout()

		elif self.cajaAgregar.IsShown(self.cajaConf3) == True:
			self.cajaAgregar.Show(self.cajaConf3, False)
			self.cajaAgregar.Show(self.cajaConf4, False)
			self.cajaAgregar.Show(self.cajaConf5, False)
			self.cajaAgregar.Show(self.cajaConf0, True)
			self.cajaAgregar.Show(self.cajaConf2, True)
			if pantComp != True:
				self.cajaConf0.Show(self.cajaResol, False)
			if mapear != True:
				self.cajaConf0.Show(self.cajaJoyst, False)
			self.cajaAgregar4.Show(self.bt_Guardar, False)
			self.cajaAgregar4.Show(self.bt_Sig, True)
			self.cajaAgregar4.Layout()
			self.cajaConf0.Layout()
			self.cajaAgregar.Layout()

	def Instalar(self, event):
		rutaArch = confiDir
		aInstalar = self.parent.GetPage(2).textIsoInst.GetValue()
		aInstalar2 = self.parent.GetPage(2).textIsoInst2.GetValue()
		aInstalar3 = self.parent.GetPage(2).textIsoInst3.GetValue()
		aInstalar4 = self.parent.GetPage(2).textIsoInst4.GetValue()
		aInstalar5 = self.parent.GetPage(2).textIsoInst5.GetValue()
		depur = self.parent.GetPage(2).ch_depur.GetValue()
		nomBase = os.path.basename(aInstalar)
		nomArch = nomBase.split(".")
		nomConf = nomArch[0]
		formato = nomArch[-1]
		tamDisco = self.parent.GetPage(2).cb_AumentarIns.GetValue()
		instalador = self.parent.GetPage(2).textInstalador.GetValue()

		if os.path.exists(rutaArch) == False:
			os.mkdir(rutaArch)

		self.rutaInst = rutaArch + '/' + nomConf + '_temp.conf'
		self.rutaError = inicio + "/ERROR.TXT"
		autoexec = "[dos]\nkeyboardlayout = sp\n"
		if self.parent.GetPage(2).ch_Aumentar.GetValue() == True:
			autoexec += "[autoexec]\nmount C " + inicio + " -freesize " + tamDisco + "\nC:\n"
		else:
			autoexec += "[autoexec]\nmount C " + inicio + "\nC:\n"

		if formato == "ima" or formato == "IMA" or formato == "img" or formato == "IMG":
			extension = "floppy"
		else:
			extension = "iso"

		if aInstalar != "" or aInstalar2 != "" or aInstalar3 != "" or aInstalar4 != "" or aInstalar5 != "":
			if aInstalar != "":
				montar = "imgmount D " + aInstalar
				if aInstalar2 != "":
					montar += " " + aInstalar2
					if aInstalar3 != "":
						montar += " " + aInstalar3
						if aInstalar4 != "":
							montar += " " + aInstalar4
							if aInstalar5 != "":
								montar += " " + aInstalar5
				montar += " -t " + extension

		autoexec += montar
		autoexec += "\nD: \n" + instalador + " 2> ERROR.TXT"
		if depur == False:
			autoexec += "\nexit"
		with open(self.rutaInst, 'w') as archivo:
			archivo.write(autoexec)

		comando = "dosbox -conf " + inicio + "/dosbox-0.74.conf -conf " + self.rutaInst
		args = shlex.split(comando)
		subprocess.run(args, shell=False)
		
		if os.path.exists(self.rutaError) == True:
			with open(self.rutaError, "r") as texto:
				leido = texto.read()
			dlg_aviso = mensaje.aviso(self, leido, _("ERROR"), wx.OK|wx.ICON_ERROR)
			dlg_aviso.ShowModal()
			dlg_aviso.Destroy()
			os.remove(self.rutaError)
		else:
			self.parent.GetPage(2).textIsoInst.Clear()
			self.parent.GetPage(2).textIsoInst2.Clear()
			self.parent.GetPage(2).textIsoInst3.Clear()
			self.parent.GetPage(2).textIsoInst4.Clear()
			self.parent.GetPage(2).textIsoInst5.Clear()
			self.parent.GetPage(2).ch_Aumentar.SetValue(False)
			self.parent.GetPage(2).cb_AumentarIns.SetValue("512")
			self.parent.GetPage(2).cajaInstalar1.Show(self.parent.GetPage(2).subcajaInstalar2, False)
			self.parent.GetPage(2).cajaInstalar1.Show(self.parent.GetPage(2).subcajaInstalar3, False)
			self.parent.GetPage(2).cajaInstalar2.Show(self.parent.GetPage(2).subcajaInstalar4, False)
			self.parent.GetPage(2).cajaInstalar2.Show(self.parent.GetPage(2).subcajaInstalar5, False)
			self.parent.GetPage(2).cajaInstalar.Layout()
		os.remove(self.rutaInst)

	def explorar(self, event):
		rutaArch = confiDir
		aInstalar = self.parent.GetPage(2).textIsoInst.GetValue()
		nomBase = os.path.basename(aInstalar)
		nomArch = nomBase.split(".")
		nomConf = nomArch[0]
		formato = nomArch[-1]

		if os.path.exists(rutaArch) == False:
			os.mkdir(rutaArch)

		self.rutaInst = rutaArch + '/' + nomConf + '_temp.conf'
		self.rutaExplo = inicio + "/DIR.TXT"

		autoexec = "[dos]\nkeyboardlayout = sp\n"
		autoexec += "[autoexec]\nmount C " + inicio + "\nC:\n"

		if formato == "ima" or formato == "IMA" or formato == "img" or formato == "IMG":
			extension = "floppy"
		else:
			extension = "iso"

		if aInstalar != "":
			montar = "imgmount D " + aInstalar
			montar += " -t " + extension

		autoexec += montar
		autoexec += "\nD: \n" + "DIR > C:DIR.TXT" + "\nexit"
		with open(self.rutaInst, 'w') as archivo:
			archivo.write(autoexec)
		
		comando = "dosbox -conf " + inicio + "/dosbox-0.74.conf -conf " + self.rutaInst
		args = shlex.split(comando)
		subprocess.run(args, shell=False)
		encontrados = []
		if os.path.exists(self.rutaExplo) == True:
			with open(self.rutaExplo, "r") as texto:
				leido = texto.readlines()
				for i in leido:
					a = i.split()
					if len(a[0]) < 9 and len(a[0]) > 1 and a[1] != "<DIR>":
						encontrados.append(a[0])

			dlg = wx.SingleChoiceDialog(self, _('Files found'), _('Which one?'), encontrados, wx.CHOICEDLG_STYLE)
			if dlg.ShowModal() == wx.ID_OK:
				elegido = dlg.GetStringSelection()
				self.parent.GetPage(2).textInstalador.SetValue(elegido)
			dlg.Destroy()
			os.remove(self.rutaExplo)

		os.remove(self.rutaInst)

	def CrearConf(self):
		#[sdl]
		a = self.ch_FullScr.GetValue()
		b = self.cb_Resol.GetValue()
		c = self.cb_Salida.GetValue()
		c2 = self.ch_Mapeo.GetValue()
		#[render]
		d = self.ch_Aspect.GetValue()
		e = self.cb_Escalado.GetValue()
		#[cpu]
		f = self.cb_Ciclos.GetValue()
		g = self.textCicMax.GetValue()
		h = self.textFijo.GetValue()
		#[autoexec]
		k = self.ch_Montar.GetValue()
		l = self.textIso.GetValue()
		ll = self.cb_Kb.GetValue()
		ms = self.ch_Ems.GetValue()
		self.formato = l.split(".")
		self.formato = self.formato[-1]
		discoTam = self.cb_AumentarConf.GetValue()

		confiJuego = configparser.RawConfigParser()
		
		aMontar = self.textRuta.GetValue()
		nombEjec = os.path.basename(aMontar)
		nomEjec = nombEjec.split('.')
		nombreJuego = nomEjec[0]
		self.rutaJuego = os.path.dirname(aMontar)
		dirEjec = os.path.relpath(self.rutaJuego, inicio)
		self.rutaConf = confiDir
		
		if a == True or c != "" or c2 != "":
			dicSdl = {}
			if a == True:
				dicSdl.update({"fullscreen": "true", "fullresolution": b})
			if c != "":
				dicSdl.update({"output": c})
			if c2 == True:
				dicSdl.update({"mapperfile": confiDir + "/" + nombreJuego + ".map"})
		if a == False:
			dicSdl.update({"fullscreen": "false"})
		confiJuego["sdl"] = dicSdl
		dicDosbox = {}
		rutacapt = self.textRuta.GetValue()
		rutacapt = os.path.dirname(rutacapt)
		rutacapt = rutacapt + "/capturas"
		dicDosbox.update({"captures": rutacapt})
		confiJuego["dosbox"] = dicDosbox
		if d == True or e != "":
			dicRender = {}
			if d == True:
				dicRender.update({"aspect": "true"})
			else:
				dicRender.update({"aspect": "false"})
			if e != "":
				dicRender.update({"scaler": e})
			confiJuego["render"] = dicRender
		dicCpu = {}
		if f != "auto":
			if f == "max":
				dicCpu = {}
				dicCpu.update({f"cycles": "max " + g + "%"})
			if f == "fixed":
				dicCpu.update({"cycles": h})
		else:
			dicCpu.update({"cycles": "auto"})
		confiJuego["cpu"] = dicCpu
		if ll != "" or ms == True:
			dicDos = {}
			if ll != "":
				ll = ll.split("-->")
				ll = ll[0]
				dicDos.update({"keyboardlayout": ll})
			if ms == True:
				dicDos.update({"Ems": "false"})
			confiJuego["dos"] = dicDos

		

		if os.path.exists(self.rutaConf) == False:
			os.mkdir(self.rutaConf)
		self.rutaConf = self.rutaConf + '/' + nombreJuego + '.conf'
		with open(self.rutaConf, 'w') as archivo:
			confiJuego.write(archivo)
		if self.ch_AumentarConf.GetValue() == True:
			autoexec = "[autoexec]\nmount C " + inicio + " -freesize " + discoTam + "\nC:\n"
		else:
			autoexec = "[autoexec]\nmount C " + inicio + "\nC:\n"

		if k == True:
			if self.formato == "ima" or self.formato == "IMA" or self.formato == "img" or self.formato == "IMG":
				extension = "floppy"
			else:
				extension = "iso"
			
			montar = "imgmount D " + l + " -t " + extension
			autoexec += montar

		autoexec += "\ncd " + dirEjec + "\n" + nombEjec + "\nexit"
		with open(self.rutaConf, 'a') as archivo:
			archivo.write(autoexec)
		if c2 == True and os.path.exists(confiDir + "/temporal.map"):
			os.rename(confiDir + "/temporal.map", confiDir + "/" + nombreJuego + ".map")

	def VerCiclos(self, e):
		if self.cb_Ciclos.GetValue() == "max":
			self.cajaCiclos.Show(self.cajaCiclosMax, True)
			self.cajaCiclos.Hide(self.cajaCiclosFijo, True)
			self.cajaConf3.Layout()
			self.cajaAgregar.Layout()
		elif self.cb_Ciclos.GetValue() == "fixed":
			self.cajaCiclos.Show(self.cajaCiclosFijo, True)
			self.cajaCiclos.Show(self.cajaCiclosMax, False)
			self.cajaConf3.Layout()
			self.cajaAgregar.Layout()
		else:
			self.cajaCiclos.Hide(self.cajaCiclosMax, True)
			self.cajaCiclos.Hide(self.cajaCiclosFijo, True)
			self.cajaConf3.Layout()
			self.cajaAgregar.Layout()

	def VerResol(self, e):
		pantComp = self.ch_FullScr.GetValue()
		if pantComp == False:
			self.cajaConf0.Show(self.cajaResol, False)
			self.cajaConf0.Layout()
			self.cajaAgregar.Layout()
		elif pantComp == True:
			self.cajaConf0.Show(self.cajaResol, True)
			self.cajaConf0.Layout()
			self.cajaAgregar.Layout()

	def VerMapeo(self, e):
		mapeo = self.ch_Mapeo.GetValue()
		if mapeo == False:
			self.cajaConf0.Show(self.cajaJoyst, False)
			self.cajaConf0.Layout()
			self.cajaAgregar.Layout()
		if mapeo == True:
			self.cajaConf0.Show(self.cajaJoyst, True)
			self.cajaConf0.Layout()
			self.cajaAgregar.Layout()

	def SelImagen(self, event):
		bt_Id = event.GetEventObject().GetId()
		bt_IsoId = self.bt_Iso.GetId()
		bt_IsoInstId = self.parent.GetPage(2).bt_IsoInst.GetId()
		bt_IsoInstId2 = self.parent.GetPage(2).bt_IsoInst2.GetId()
		bt_IsoInstId3 = self.parent.GetPage(2).bt_IsoInst3.GetId()
		bt_IsoInstId4 = self.parent.GetPage(2).bt_IsoInst4.GetId()
		bt_IsoInstId5 = self.parent.GetPage(2).bt_IsoInst5.GetId()
		seguir = "si"

		if bt_Id == bt_IsoInstId2:
			formato = self.parent.GetPage(2).textIsoInst.GetValue()
			formato = formato.split(".")
			formato = formato[-1]
			if formato == "ima" or formato == "IMA" or formato == "img" or formato == "IMG":
				dlg_aviso = mensaje.aviso(self, _("Using multiple files is only supported for cue/iso images"), _("Information"), wx.OK|wx.ICON_INFORMATION)
				self.parent.GetPage(2).cajaInstalar1.Show(self.parent.GetPage(2).subcajaInstalar2, False)
				dlg_aviso.ShowModal()
				dlg_aviso.Destroy()
				seguir = "No"

		if seguir == "si":
			try:
				dlgagr = wx.FileDialog(self, _("Select an image file"), inicio, "", "*.iso;*.ISO;*.cue;*.CUE;*.img;*.IMG;*.ima;*.IMA")
				if dlgagr.ShowModal() == wx.ID_OK:
					self.rutaImagen = dlgagr.GetPath()
					self.archImagen = os.path.basename(self.rutaImagen)
					self.carpeta = os.path.dirname(self.rutaImagen)
					self.carpeta = self.carpeta.split('/')
					self.nomConf = self.archImagen.split(".")
					if bt_Id == bt_IsoId:
						self.textIso.SetValue(self.rutaImagen)
					if bt_Id == bt_IsoInstId:
						self.parent.GetPage(2).textIsoInst.SetValue(self.rutaImagen)
						self.parent.GetPage(2).cajaInstalar1.Show(self.parent.GetPage(2).subcajaInstalar2, True)
					if bt_Id == bt_IsoInstId2:
						self.parent.GetPage(2).textIsoInst2.SetValue(self.rutaImagen)
						self.parent.GetPage(2).cajaInstalar1.Show(self.parent.GetPage(2).subcajaInstalar3, True)
						dlg_aviso = mensaje.aviso(self, _("Remember: CTRL-F4 Switch between disks"), _("INFORMATION"), wx.OK|wx.ICON_INFORMATION)
						dlg_aviso.ShowModal()
						dlg_aviso.Destroy()
					if bt_Id == bt_IsoInstId3:
						self.parent.GetPage(2).textIsoInst3.SetValue(self.rutaImagen)
						self.parent.GetPage(2).cajaInstalar2.Show(self.parent.GetPage(2).subcajaInstalar4, True)
					if bt_Id == bt_IsoInstId4:
						self.parent.GetPage(2).textIsoInst4.SetValue(self.rutaImagen)
						self.parent.GetPage(2).cajaInstalar2.Show(self.parent.GetPage(2).subcajaInstalar5, True)
					if bt_Id == bt_IsoInstId5:
						self.parent.GetPage(2).textIsoInst5.SetValue(self.rutaImagen)

					self.parent.GetPage(2).cajaInstalar.Layout()
					
				dlgagr.Destroy()
			except:
				wx.MessageBox(message = _("First of all, select your working directory"), caption=_('Help'), style=wx.OK | wx.ICON_INFORMATION)

	def Agregar(self, event):
		try:
			dlgagr = wx.FileDialog(self, _("Select the game executable"), inicio, "", "*.*")
			if dlgagr.ShowModal() == wx.ID_OK:
				self.rutaejec = dlgagr.GetPath()
				self.ejecu = os.path.basename(self.rutaejec)
				self.textRuta.SetValue(self.rutaejec)
			dlgagr.Destroy()
		except:
			wx.MessageBox(message = _("First of all, select your working directory"), caption=_('Help'), style=wx.OK | wx.ICON_INFORMATION)

	def Guardar(self, event):
		rutaGuardar = self.textRuta.GetValue()
		dirEjecu = os.path.dirname(rutaGuardar)
		nomGuardar = os.path.basename(rutaGuardar)
		nomCort = nomGuardar.split(".")
		nomArchGuard = nomCort[0]
		formatoGuardar = nomCort[-1]

		if self.textNombre.GetValue() != "" and nomGuardar != "":
			if self.listado.GetFirstSelected() != -1:
				indice = event.GetSelection()
				juego = self.listado.GetFirstSelected(indice)
				nomJuego = self.listado.GetItemText(juego)
				control = True
				control2 = False
				self.Borrar(nomJuego, control, control2)
			control = False

			self.CrearConf()
			GuarScr = str(self.ch_FullScr.GetValue())
			GuarMap = str(self.ch_Mapeo.GetValue())
			GuarAspect = str(self.ch_Aspect.GetValue())
			GuarMontar = str(self.ch_Montar.GetValue())
			espacio = str(self.ch_AumentarConf.GetValue())
			if self.textDescrip.GetValue() == "":
				valorDescrip = _("Without description")
			else:
				valorDescrip = self.textDescrip.GetValue()
			if self.rutaConf == "":
				self.rutaConf = _("No configuration file")
			if self.ch_FullScr.GetValue() == False:
				pantalla = ""
			else:
				pantalla = self.cb_Resol.GetValue()
			if self.cb_Ciclos.GetValue() == "auto":
				maxCiclos = ""
				fijCiclos = ""
			elif self.cb_Ciclos.GetValue() == "fixed":
				maxCiclos = ""
				fijCiclos = self.textFijo.GetValue()
			elif self.cb_Ciclos.GetValue() == "max":
				maxCiclos = self.textCicMax.GetValue()
				fijCiclos = ""
			if self.ch_Montar.GetValue() == True:
				montarimag = self.textIso.GetValue()
			else:
				montarimag = ""
			if self.ch_AumentarConf.GetValue() == True:
				espacioDisco = self.cb_AumentarConf.GetValue()
			else:
				espacioDisco = ""
			if self.ch_Ems.GetValue() == True:
				ems = "True"
			else:
				ems = "False"
			self.midic = {}
			self.midic.update({"nombre": self.textNombre.GetValue(), "descripcion": valorDescrip, "ejecutable": nomGuardar, "ruta": rutaGuardar, "ruconf": self.rutaConf, "fullscr": GuarScr, "resolucion": pantalla, "salida": self.cb_Salida.GetValue(), "mapear": GuarMap, "aspecto": GuarAspect, "escalado": self.cb_Escalado.GetValue(), "ciclos": self.cb_Ciclos.GetValue(), "maxciclos": maxCiclos, "fijciclos": fijCiclos, "montar": GuarMontar,"mapateclas": self.cb_Kb.GetValue(), "Ems": ems, "imagen": montarimag, "Espacio": espacio, "EspacioDisco": espacioDisco })
			# Segunda imagen para montar en el diccionario despues de "imagen": montarimag: "imagen2": montarimag2,
			global dicci_Juegos
			
			confi_Juegos[0] = inicio
			dicci_Juegos.append(self.midic)
			dicci_Juegos = sorted(dicci_Juegos, key=lambda k: k['nombre'], reverse = True)
			confi_Juegos[1] = dicci_Juegos
			configuracion = open(confiRuta, "wb")
			pickle.dump(confi_Juegos, configuracion)
			configuracion.close()
			self.listado.DeleteAllItems()
			i = 0
			while i < len(dicci_Juegos):
				self.listado.InsertItem(0, dicci_Juegos[i].get("nombre"))
				i += 1
			self.Limpiar()
			self.ejecu = ""
			self.ruconf = ""
		else:
			if self.textNombre.GetValue() == "":
				mensAlerta = _("The name field is required")
			else:
				mensAlerta = _("Select a file")

			dlg_aviso = mensaje.aviso(self, _(mensAlerta), _("INFORMATION"), wx.OK|wx.ICON_INFORMATION)
			dlg_aviso.ShowModal()
			dlg_aviso.Destroy()

			self.textNombre.Clear()
			self.ejecu = ""
			self.textDescrip.Clear()

	def Borrar(self, nomJuego, control, control2):
		i = 0
		while i < len(dicci_Juegos):
			for clave_juegos in dicci_Juegos[i]:
				if dicci_Juegos[i]["nombre"] == nomJuego:
					borrarConf = dicci_Juegos[i].get("ruconf")
					os.remove(borrarConf)
					del dicci_Juegos[i]
					break
			i += 1

		self.listado.DeleteAllItems()
		i = 0
		while i < len(dicci_Juegos):
			for clave_juegos in dicci_Juegos[i]:
				if clave_juegos == "nombre":
					self.listado.InsertItem(0, dicci_Juegos[i].get("nombre"))
			i += 1
		confi_Juegos[0] = inicio
		confi_Juegos[1] = dicci_Juegos
		configuracion = open(confiRuta, "wb")
		pickle.dump(confi_Juegos, configuracion)
		configuracion.close()

		if control == False:
			self.Limpiar()
		if control2 == True:
			self.parent.GetPage(0).Actualizar(nomJuego)

	def Limpiar(self):
		juego = self.listado.GetFirstSelected()
		self.listado.SetItemState(juego, 0, wx.LIST_STATE_SELECTED)
		self.textNombre.Clear()
		self.textDescrip.Clear()
		self.textRuta.Clear()
		self.ch_FullScr.SetValue(False)
		self.cb_Resol.SetValue("")
		self.cb_Salida.SetValue("")
		self.ch_Aspect.SetValue(False)
		self.cb_Escalado.SetValue("")
		self.cb_Ciclos.SetValue("")
		self.textCicMax.Clear()
		self.textFijo.Clear()
		self.cb_Kb.SetValue("")
		self.ch_Montar.SetValue(False)
		self.textIso.Clear()
		self.ch_AumentarConf.SetValue(False)
		self.cb_AumentarConf.SetValue("512")
		self.ch_Ems.SetValue(False)
		self.ch_Mapeo.SetValue(False)

class frame_principal(wx.Frame):
	def __init__(self, parent):
		wx.Frame.__init__(self, parent, title = "Boxdos", size = (800, 1200))
		self.panel = wx.Notebook(self, -1)
		self.SetIcon(wx.Icon('/usr/share/boxdos/data/iconos/icono48.png', wx.BITMAP_TYPE_PNG))

		self.traduccion = gettext.translation("boxdos", "/usr/share/locale", languages=idiomas, fallback=True)
		self.traduccion.install()

		#Pantalla de bienvenida
		imagePath = "/usr/share/boxdos/data/img/splash.png"
		bitmap = wx.Bitmap(imagePath, wx.BITMAP_TYPE_PNG)
		shadow = wx.WHITE
		splash = AS.AdvancedSplash(None, bitmap=bitmap, timeout=3000, agwStyle=AS.AS_TIMEOUT | AS.AS_CENTER_ON_PARENT | AS.AS_SHADOW_BITMAP, shadowcolour=shadow)
		font = wx.Font(wx.FontInfo(10).FaceName("").Italic())
		splash.SetTextFont(font)
		splash.SetText("BoxDos-0.8.2")

		sizer = wx.BoxSizer()

		global dirs

		# Hojas.
		hoja1 = Juegos(self.panel, _("List"))
		hoja2 = AgregarBorrar(self.panel, _("Add/Edit"))
		hoja3 = instalar.Inst(self.panel, _("Install"), aumentos, confiDir, inicio)
		hoja4 = ayuda.Visor(self.panel, _("Help"))

		# Añadimos las hojas al NoteBook.
		self.panel.AddPage(hoja1, _("List"))
		self.panel.AddPage(hoja2, _("Add/Edit"))
		self.panel.AddPage(hoja3, _("Install"))
		self.panel.AddPage(hoja4, _("Help"))

		self.panel.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.cambioPag)
		self.panel.GetPage(2).Bind(wx.EVT_CHECKBOX, self.panel.GetPage(1).MasDisco, self.panel.GetPage(2).ch_Aumentar)
		self.panel.GetPage(2).Bind(wx.EVT_BUTTON, self.panel.GetPage(1).SelImagen, self.panel.GetPage(2).bt_IsoInst)
		self.panel.GetPage(2).Bind(wx.EVT_BUTTON, self.panel.GetPage(1).SelImagen, self.panel.GetPage(2).bt_IsoInst2)
		self.panel.GetPage(2).Bind(wx.EVT_BUTTON, self.panel.GetPage(1).SelImagen, self.panel.GetPage(2).bt_IsoInst3)
		self.panel.GetPage(2).Bind(wx.EVT_BUTTON, self.panel.GetPage(1).SelImagen, self.panel.GetPage(2).bt_IsoInst4)
		self.panel.GetPage(2).Bind(wx.EVT_BUTTON, self.panel.GetPage(1).SelImagen, self.panel.GetPage(2).bt_IsoInst5)
		self.panel.GetPage(2).bt_Instalar.Bind(wx.EVT_BUTTON, self.panel.GetPage(1).Instalar)
		self.panel.GetPage(2).bt_explor.Bind(wx.EVT_BUTTON, self.panel.GetPage(1).explorar)

		sizer.Add(self.panel, 1, wx.EXPAND|wx.ALL, 5)

		self.SetSizer(sizer)

	def cambioPag(self, event):
		pesta = event.GetSelection()
		if pesta == 1:
			self.panel.GetPage(1).Limpiar()
			self.panel.GetPage(0).Actualizar(event)
		if pesta == 0:
			self.panel.GetPage(0).Actualizar(event)

app = wx.App()
frame = frame_principal(None)
frame.Show()
#frame.Maximize(True)
app.MainLoop()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2019 - 2025 (C) https://myc-git.gitlab.io/mycweb/ (Jose Antonio Carrascosa García)
#
# This file is part of Boxdos
#
# Boxdos is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 3 of the License, or 
# (at your option) any later version.
#
# Boxdos is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import wx
import wx.html
import wx.adv

class Visor(wx.Panel):

	def __init__(self, parent, nombre_hoja):
		# Constructor.
		wx.Panel.__init__(self, parent)
		sizerWeb = wx.StaticBoxSizer(wx.HORIZONTAL, self)
		self.html = wx.html.HtmlWindow(self)

		if "gtk2" in wx.PlatformInfo or "gtk3" in wx.PlatformInfo: 
			self.html.SetStandardFonts()

		self.html.LoadPage("file:///usr/share/doc/boxdos/html/boxdos.html")
		#self.html.LoadPage("file://../share/doc/boxdos/html/boxdos.html")

		sizerWeb.Add(self.html, 1, wx.ALL|wx.EXPAND|wx.ALL, 5)
		self.SetSizer(sizerWeb)
		self.html.Bind(wx.html.EVT_HTML_LINK_CLICKED, self.AcercaDe)

	def AcercaDe(self, event):
		href = event.GetLinkInfo().GetHref()
		if href == "#acercade":
		
			with open('/usr/share/common-licenses/GPL-3', "r") as texto:
				licencia = texto.read()
			
			descripcion=""" Boxdos es una ayuda gráfica para instalar, configurar y lanzar programas o juegos en dosbox. """
			info = wx.adv.AboutDialogInfo()
			icono2 = wx.Icon("/usr/share/boxdos/data/iconos/icono2.png")
			#icono2 = wx.Icon("share/boxdos/data/iconos/icono2.png")
			info.SetIcon(icono2)
			info.SetName('Boxdos')
			info.SetDescription(descripcion)
			info.SetVersion('0.8.2')
			info.SetLicense(licencia)
			info.SetDevelopers(['Jose Antonio Carrascosa García'])
			info.SetWebSite('https://myc-git.gitlab.io/mycweb/')
			info.SetCopyright('(c) 2019-2025')
			info.SetArtists(['Jose Antonio Carrascosa García'])
			wx.adv.AboutBox(info)
		elif href == "#inicio":
			self.html.LoadPage("#inicio")
		elif href == "#instalar":
			self.html.LoadPage("#instalar")
		elif href == "#editar":
			self.html.LoadPage("#editar")
		elif href == "#lanzar":
			self.html.LoadPage("#lanzar")
		elif href == "#menu":
			self.html.LoadPage("#menu")


#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2019 - 2025 (C) https://myc-git.gitlab.io/mycweb/ (Jose Antonio Carrascosa García)
#
# This file is part of Boxdos
#
# Boxdos is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 3 of the License, or 
# (at your option) any later version.
#
# Boxdos is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import wx
import os

class Inst(wx.Panel):
	def __init__(self, parent, nombre_hoja, aumentos, confiDir, inicio):
		# Constructor.
		wx.Panel.__init__(self, parent)
		# Sizers.
		self.inicio = inicio
		self.confiDir = confiDir
		sizerInst = wx.StaticBoxSizer(wx.HORIZONTAL, self)
		self.cajaInstalar = wx.StaticBoxSizer(wx.VERTICAL, self, _("Install"))

		self.cajaInstalar1 = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaInstalar2 = wx.BoxSizer(wx.HORIZONTAL)

		self.subcajaInstalar = wx.BoxSizer(wx.HORIZONTAL)
		self.subcajaInstalar2 = wx.BoxSizer(wx.HORIZONTAL)

		self.subcajaInstalar3 = wx.BoxSizer(wx.HORIZONTAL)
		self.subcajaInstalar4 = wx.BoxSizer(wx.HORIZONTAL)
		self.subcajaInstalar5 = wx.BoxSizer(wx.HORIZONTAL)

		self.subcajaInstalar6 = wx.BoxSizer(wx.VERTICAL)
		self.subcajaInstalar7 = wx.BoxSizer(wx.HORIZONTAL)
		self.cajaBotonInst = wx.BoxSizer(wx.HORIZONTAL)

		self.s_textIsoInst = wx.StaticText(self, -1, _("Image:"))
		self.textIsoInst = wx.TextCtrl(self, -1, size = (150, 35))

		self.s_textIsoInst2 = wx.StaticText(self, -1, _("2ª Image:"))
		self.textIsoInst2 = wx.TextCtrl(self, -1, size = (150, 35))

		self.s_textIsoInst3 = wx.StaticText(self, -1, _("3ª Image:"))
		self.textIsoInst3 = wx.TextCtrl(self, -1, size = (150, 35))
		self.s_textIsoInst4 = wx.StaticText(self, -1, _("4ª Image:"))
		self.textIsoInst4 = wx.TextCtrl(self, -1, size = (150, 35))
		self.s_textIsoInst5 = wx.StaticText(self, -1, _("5ª Image:"))
		self.textIsoInst5 = wx.TextCtrl(self, -1, size = (150, 35))

		self.bt_IsoInst = wx.Button(self, -1, label = _("Open"))
		self.bt_IsoInst2 = wx.Button(self, -1, label = _("Open"))

		self.bt_IsoInst3 = wx.Button(self, -1, label = _("Open"))
		self.bt_IsoInst4 = wx.Button(self, -1, label = _("Open"))
		self.bt_IsoInst5 = wx.Button(self, -1, label = _("Open"))
		self.bt_explor = wx.Button(self, -1, label = _("Explore"))
		self.ch_Aumentar = wx.CheckBox(self, -1, _("Increase disk size"))
		

		self.cajaAumentarins = wx.BoxSizer(wx.HORIZONTAL)

		self.cb_AumentarIns = wx.ComboBox(self, -1, value = aumentos[0], choices = aumentos, style=wx.CB_READONLY, size = (150, 35))

		self.s_textInstalador = wx.StaticText(self, -1, _("Installer executable:"))
		self.textInstalador = wx.TextCtrl(self, -1, size = (130, 30))
		
		self.bt_Instalar = wx.Button(self, -1, label = _("Install"))
		self.ch_depur = wx.CheckBox(self, -1, _("Do not close DOSBox."))

		self.cajaInstalar.Add(self.cajaInstalar1, 0, wx.EXPAND|wx.ALL,5)
		self.cajaInstalar.Add(self.cajaInstalar2, 0, wx.EXPAND|wx.ALL,5)

		self.cajaInstalar1.Add(self.subcajaInstalar, 0, wx.EXPAND|wx.ALL,5)
		self.cajaInstalar1.Add(self.subcajaInstalar2, 0, wx.EXPAND|wx.ALL,5)
		self.cajaInstalar1.Add(self.subcajaInstalar3, 0, wx.EXPAND|wx.ALL,5)
		self.cajaInstalar2.Add(self.subcajaInstalar4, 0, wx.EXPAND|wx.ALL,5)
		self.cajaInstalar2.Add(self.subcajaInstalar5, 0, wx.EXPAND|wx.ALL,5)

		self.cajaInstalar.Add(self.subcajaInstalar6, 0, wx.EXPAND|wx.ALL,5)
		self.cajaInstalar.Add(self.subcajaInstalar7, 0, wx.EXPAND|wx.ALL,5)
		self.cajaInstalar.Add(self.cajaBotonInst, 0, wx.EXPAND|wx.ALL,5)

		self.subcajaInstalar.Add(self.s_textIsoInst, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar.Add(self.textIsoInst, 1, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar.Add(self.bt_IsoInst, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar2.Add(self.s_textIsoInst2, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar2.Add(self.textIsoInst2, 1, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar2.Add(self.bt_IsoInst2, 0, wx.EXPAND|wx.ALL,5)

		self.subcajaInstalar3.Add(self.s_textIsoInst3, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar3.Add(self.textIsoInst3, 1, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar3.Add(self.bt_IsoInst3, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar4.Add(self.s_textIsoInst4, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar4.Add(self.textIsoInst4, 1, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar4.Add(self.bt_IsoInst4, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar5.Add(self.s_textIsoInst5, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar5.Add(self.textIsoInst5, 1, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar5.Add(self.bt_IsoInst5, 0, wx.EXPAND|wx.ALL,5)

		self.subcajaInstalar6.Add(self.ch_Aumentar, 0, wx.EXPAND|wx.ALL,5)
		self.cajaAumentarins.Add(self.cb_AumentarIns, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar6.Add(self.cajaAumentarins, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar7.Add(self.s_textInstalador, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar7.Add(self.textInstalador, 0, wx.EXPAND|wx.ALL,5)
		self.subcajaInstalar7.Add(self.bt_explor, 0, wx.EXPAND|wx.ALL,5)
		self.cajaBotonInst.Add(self.bt_Instalar, 0, wx.EXPAND|wx.ALL,5)
		self.cajaBotonInst.Add(self.ch_depur, 0, wx.EXPAND|wx.ALL,5)

		self.subcajaInstalar6.Show(self.cajaAumentarins, False)

		self.cajaInstalar1.Show(self.subcajaInstalar2, False)
		self.cajaInstalar1.Show(self.subcajaInstalar3, False)
		self.cajaInstalar2.Show(self.subcajaInstalar4, False)
		self.cajaInstalar2.Show(self.subcajaInstalar5, False)
		
		self.textInstalador.SetValue("INSTALL")
		
		sizerInst.Add(self.cajaInstalar, 1, wx.ALL|wx.EXPAND|wx.ALL,5)
		self.SetSizer(sizerInst)

